# sample.py
import falcon
import json
from pymongo import MongoClient
import sys
import os
sys.path.append('./ML/DBSCAN/')
from ML.DBSCAN.helper_function import haversine

'''
function to populate the old-style database with the old-style data entry
'''
def populate_user_data_db(body):    
    client = MongoClient() 
    db = client.bodhi #bodhi is the db name
    latitudes = body["latitude"]
    longitudes = body["longitude"]
    timestamps = body["timestamp"]
    altitudes = body["altitude"]
    directions = body["direction"]
    speeds = body["speed"]
    email_id = body["email"]
    dev_id = body["deviceId"]
    cursor = db.user_data.find({"email": email_id})
    if (cursor.count()>0):
        db.user_data.update({"email": email_id}, {"$push": {"latitude": {"$each": latitudes}}})   
        db.user_data.update({"email": email_id}, {"$push": {"timestamp": {"$each": timestamps}}})  
        db.user_data.update({"email": email_id}, {"$push": {"longitude": {"$each": longitudes}}})
        db.user_data.update({"email": email_id}, {"$push": {"altitude": {"$each": altitudes}}})
        db.user_data.update({"email": email_id}, {"$push": {"direction": {"$each": directions}}})
        db.user_data.update({"email": email_id}, {"$push": {"speed": {"$each": speeds}}})
    else:
        db.user_data.insert_one(body)
    
    os.system("python ./ML/DBSCAN/dbscan_main.py %s" % email_id) #running the clustering algo with newly added data in the database
        
        
'''
function to populate the old-style database with the new-style data entry
...old-style... : where each parameter (lat, long, velocity, ...) has its own list --> good for analytics
...new-style... : where each data entry set (set of one lat, one long, one velocity...) is an entry in the data list --> good for view
'''
def populate_user_data_db_column_major(body):    
    client = MongoClient() 
    db = client.bodhi #bodhi is the db name
    latitudes = []
    longitudes = []
    timestamps = []
    altitudes = []
    directions = []
    speeds = []
    email_id = body["email"]
    dev_id = body["deviceId"]
    
    for ts in body["locations"].keys():
        latitudes.append(body["locations"][ts][0])
        longitudes.append(body["locations"][ts][1])
        timestamps.append(ts)
        altitudes.append(body["locations"][ts][2])
        directions.append(body["locations"][ts][4])
        speeds.append(body["locations"][ts][3])
    
    cursor = db.user_data.find({"email": email_id})
    if (cursor.count()>0):
        db.user_data.update({"email": email_id}, {"$push": {"latitude": {"$each": latitudes}}})   
        db.user_data.update({"email": email_id}, {"$push": {"timestamp": {"$each": timestamps}}})  
        db.user_data.update({"email": email_id}, {"$push": {"longitude": {"$each": longitudes}}})
        db.user_data.update({"email": email_id}, {"$push": {"altitude": {"$each": altitudes}}})
        db.user_data.update({"email": email_id}, {"$push": {"direction": {"$each": directions}}})
        db.user_data.update({"email": email_id}, {"$push": {"speed": {"$each": speeds}}})
    else:
        db.user_data.insert_one(body)
    
    os.system("python ./ML/DBSCAN/dbscan_main.py %s" % email_id) #running the clustering algo with newly added data in the database

   


'''
function to populate the new-style database with the new-style data entry
'''
def populate_user_data_db_row_major(body):    
    client = MongoClient() 
    db = client.bodhi #bodhi is the db name   
    email_id = body["email"]
    dev_id = body["deviceId"]
    
    """
    response:{
    locations:{
    timestamp: [lat,long,alt,timestamp,speed,bearing],
    timestamp:
     [lat,long,alt,timestamp,speed,bearing]
    }
    }
    """   
    
    cursor = db.user_data_modular.find({"email": email_id})
    if (cursor.count()>0):
        for ts in body["locations"].keys():
            db.user_data_modular.update({"email": email_id}, {"$set": {"locations.%s" % ts : body["locations"][ts]}})               
    else:
        db.user_data_modular.insert_one(body)
    


    
class QueryHandler:
    
    def on_post(self, req, resp):
        """with open('test.json', 'w') as f:
            while True:
                chunk = req.stream.read(4096)
                if not chunk:
                    break
                f.write(chunk)"""
        body = req.stream.read()
        body = json.loads(body) #body has the json data
        
        if data_check_pass(body):
            #curently getting old-style data and populating only old-style db
            populate_user_data_db(body)
        
        print(body)
        resp.status = falcon.HTTP_201
        
app = falcon.API()

app.add_route('/ping', QueryHandler())