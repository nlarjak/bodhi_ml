import csv
import numpy as np
from pylab import *
from helper_functions import *
import sklearn
from sklearn.cluster import DBSCAN
import json
from pymongo import MongoClient
import sys
from collections import OrderedDict


def myDBSCAN(params):
    
    #DEVICE_ID = params["dev_id"]
    EPS = params["eps"]
    MINPTS = params["minpts"]
    NOISE = params["noise_id"]
    MIN_DURATION = params["min_dur"]
    EMAIL_ID = params["email"]


    data = {'index': [], 'lat': [], 'long': [], 'time': [], 'cluster': []}

    #load the data from csv file into 'data'
    load_data(data, EMAIL_ID)

    #transform the data into sklearn usable format
    sklearn_data = zip(data['lat'], data['long'])

    #define the DBSCAN model using the sklearn's DBSCAN library	
    dbscan = DBSCAN(eps=EPS, min_samples = MINPTS, metric = 'haversine')

    #fit our data to the defined DBSCAN model
    dbscan.fit(sklearn_data)

    #obtain the labels (cluster_ids) for all the points of the dataset
    labels = dbscan.labels_

    #obtain the cluster_ids (length of this list is the number of clusters)
    cluster_list = list(set(labels))
    
    #load database and clear old data
    client = MongoClient() 
    db = client.bodhi
    cursor = db.user_processed_data.find({"email": EMAIL_ID})
    if (cursor.count()>0):
        db.user_processed_data.update_one({"email": EMAIL_ID}, {"$set": { "clusters":[]}})
    else:
        db.user_processed_data.insert_one({"email": EMAIL_ID, "clusters":[]})
        
        
        
    #loop over the clusters, and doing so, cache the lists of (i)cluster sizes as key, and (ii)cluster mean time (in total minutes of the day) as value
    cluster_cache = {}
    
    for cluster in cluster_list:               
        
        #check if the cluster in question is the noise category
        if (cluster==NOISE):
            continue

        #retrieve the list of latitudes, longitudes and start times of all the points belonging to the current cluster		
        cluster_lats, cluster_longs, cluster_times = get_cluster_lat_long_time(data, labels, cluster)
        
        #update cluster_cache
        cluster_cache[cluster] = {cluster_times.size : cluster_times}                                        
                
        

        
    #now sort the cluster_cache by cluster size (desending order)
    cluster_cache = OrderedDict(sorted(cluster_cache.items(), reverse=True, key = lambda k:k[1].keys()))
    
    #set up the home_work assigner dict
    home_work = {}
    cluster_count = 0
    home_flag = True
    work_flag = True
    
    
    #current logic for assigning home and work locations based on (i)cluster_size and (ii)cluster_mean_time
    
    for cluster in cluster_cache.keys():        
        
        mean_time = np.mean(cluster_cache[cluster].values()[0])
        std_time = np.std(cluster_cache[cluster].values()[0])
        
        #set of conditions for being a home/work
        home_cond1 = (std_time > 300) #for a homer who is always at home (10 hours at least)
        home_cond2 = ((mean_time > 1000) or (mean_time < 500))
        work_cond1 = ((mean_time > 500) and (mean_time < 1000))
        
        if (work_flag and work_cond1 and not(home_cond1)):
            home_work[cluster] = "Work"
            work_flag = False
        elif (home_flag and (home_cond1 or home_cond2)):
            home_work[cluster] = "Home"
            home_flag = False        
        else:
            cluster_count = cluster_count + 1        
            home_work[cluster] = "Other Location" + str(cluster_count)        
        
    
    
    #Finally update the database
    for cluster in cluster_list:
        if (cluster==NOISE):
            continue
                    
        #retrieve the list of latitudes, longitudes and start times of all the points belonging to the current cluster		
        cluster_lats, cluster_longs, cluster_times = get_cluster_lat_long_time(data, labels, cluster)
        
        print ("Updating database")
        #print the summary about the cluster latitude, longitude, number of points and start time
        update_cluster_details(db, EMAIL_ID, home_work[cluster], cluster_longs, cluster_lats, cluster_times)
        