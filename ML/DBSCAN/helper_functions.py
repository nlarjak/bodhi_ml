import numpy as np
from pylab import *
import sklearn
from sklearn.cluster import DBSCAN
import json
from pymongo import MongoClient
import sys
import pylab
from datetime import datetime
import requests

UNCLUSTERED = -2


'''
function for adding an entry to the database
'''
def add_data(data, id, lat, long, time):
        data['index'].append(id)
        data['lat'].append(lat)
        data['long'].append(long)
        data['time'].append(time)
        data['cluster'].append(UNCLUSTERED)
        
    
'''
function to retrieve the latitudes and longitudes 
of data-points that belong to the cluster
whose id is cluster_id
'''	
def get_cluster_lat_long_time(data, labels, cluster_id):
        cluster_lats = np.array(data['lat'])[(labels==cluster_id).nonzero()[0]]
        cluster_longs = np.array(data['long'])[(labels==cluster_id).nonzero()[0]]
        cluster_times= np.array(data['time'])[(labels==cluster_id).nonzero()[0]]
        return cluster_lats, cluster_longs, cluster_times
        
        
    
'''
function to load the data from the provided csv files
'''	
def load_data(data, email_id):
    client = MongoClient() 
    db = client.bodhi
    cursor = db.user_data.find({"email":email_id})
    count = 0
    for document in cursor:
        for i in range(len(document["latitude"])):
            time = transform_time(int(document["timestamp"][i]))
            add_data(data, count, float(document["latitude"][i]), float(document["longitude"][i]), time)
            count = count + 1



                     
'''
function to transform timestamp in milliseconds to 
hhmm and then finally to minutes in the day
'''                     
def transform_time(timestamp):
    dt_obj = datetime.fromtimestamp(timestamp/1000)
    minutes = dt_obj.hour*60 + dt_obj.minute
    return minutes

                     
                     
'''
function to update in db the mean and standard deviations of the 
latitudes, longitudes and starting times of each point of 
the cluster whose id is cluster_id, and also print the 
number of points in this cluster
'''
def update_cluster_details(db, email_id, cluster_id, cluster_longs, cluster_lats,cluster_times):

    mean_lat = np.mean(cluster_lats)
    std_lat = np.std(cluster_lats)
    mean_long = np.mean(cluster_longs)
    std_long = np.std(cluster_longs)
    mean_time = np.mean(cluster_times)    
    std_time = np.std(cluster_times)
    
    url_string = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + str(mean_lat) + "," + str(mean_long)    
    location_name = str(requests.get(url_string).json()["results"][0]["formatted_address"].encode("UTF-8"))
        
    db.user_processed_data.update_one({"email": email_id}, {"$push":{"clusters": {"cluster_id":cluster_id, "cluster_data":{"cluster_latitude_mean": mean_lat, "cluster_latitude_std":std_lat, "cluster_longitude_mean": mean_long, "cluster_longitude_std":std_long, "cluster_time_of_day_mean":mean_time, "cluster_time_of_day_std":std_time, "cluster_size": str(cluster_lats.size), "cluster_location_name":location_name}}}})
    
    print "Cluster - " + str(cluster_id) + " : "
    print "============================"
    print "Cluster Size (numper of points) = " + str(cluster_lats.size)
    print "Cluster Latitude = " + str(mean_lat) + " +- " + str(std_lat)
    print "Cluster Longitude = " + str(mean_long) + " +- " + str(std_long)
    print "Cluster Time (hhmm)= " + str(mean_time) + " +- " + str(std_time)
    print "============================"
    
    
    
    
    
    
'''	
function that defines the distance function - haversine
distance, used to quantify distances between two points on a 
sphere with latitudes and longitudes as (lat1, long1) and 
(lat2, long2) respectively. The final distance is expressed 
in meters
'''
def haversine(lat1, long1, lat2, long2):
    lat1, long1, lat2, long2 = map(np.radians, [lat1, long1, lat2, long2])    
    lat_diff = lat2 - lat1
    long_diff = long2 - long1
    tmp = np.sin(lat_diff/2.0)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(long_diff/2.0)**2
    dist_raw = 2 * np.arcsin(np.sqrt(tmp))	
    dist = 6367 * dist_raw	
    return dist*1000
