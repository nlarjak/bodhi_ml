import csv
import numpy as np
from pylab import *
from helper_functions import *
import sklearn
from sklearn.cluster import DBSCAN
import json
from pymongo import MongoClient
import sys
from dbscan import myDBSCAN


'''
define all the constants (parameters) - TO DO: put them in a configuration file
'''

#epsilon
EPS = 0.001

#minimum number of points in a cluster
MINPTS = 10

#id for the special cluster - "noise"
NOISE = -1

#duration filter parameter to filter out non-candidate points 
MIN_DURATION = 180 #minutes



def main_function(argv):
    
    EMAIL_ID = str(argv[0])
    
    params = {"eps":EPS, "minpts":MINPTS, "noise_id":NOISE, "min_dur":MIN_DURATION, "email":EMAIL_ID}

    myDBSCAN(params)
        
if __name__ == "__main__":
    main_function(sys.argv[1:])