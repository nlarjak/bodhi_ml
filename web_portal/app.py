from flask import Flask, render_template, request, redirect
import requests
from flask.ext.mongoengine import MongoEngine
from flask.ext.mongoengine.wtf import model_form
from wtforms import PasswordField
from flask.ext.login import LoginManager, login_user, logout_user, login_required, current_user
from pymongo import MongoClient
import json, ast
import datetime

app = Flask(__name__)

"""login_manager = LoginManager()
login_manager.init_app(app)"""

app.config["DEBUG"] = True 
app.config['SECRET_KEY'] = 'harekrishna'
app.config['WTF_CSRF_ENABLED'] = True

#app.config['MONGODB_SETTINGS'] = { 'db' : 'books' }

#db = MongoEngine(app)

"""class User(db.Document):
	name = db.StringField(required=True, unique=True)
	password = db.StringField(required=True)
	def is_authenticated(self):
		users = User.objects(name=self.name, password=self.password)
		return len(users) != 0
	def is_active(self):
		return true
	def is_anonymous(self):
		return false
	def get_id(self):
		return self.name

class FavoriteBook(db.Document):
    author = db.StringField(required=True)
    title = db.StringField(required=True)
    link = db.StringField(required=True)
    poster = db.ReferenceField(User)

@login_manager.user_loader
def load_user(name):
	users = User.objects(name=name)
	if len(users) != 0:
		return users[0]
	else:
		return None

UserForm = model_form(User)
UserForm.password = PasswordField('password')


@app.route("/")
def hello():
	return render_template("hello.html")


@app.route("/name")
def name():
	return "Ashish Ranjan Jha"


@app.route("/search", methods=["POST", "GET"])
#@login_required
def search():
	if request.method == "POST":
		url = "https://www.googleapis.com/books/v1/volumes?q=" + request.form["user_search"]
		response_dict = requests.get(url).json()
		return render_template("results.html", api_data=response_dict)
	else:
		return render_template("search.html")


@app.route("/favorite/<id>")
@login_required
def favorite(id):
	book_url = "https://www.googleapis.com/books/v1/volumes/" + id
	book_dict = requests.get(book_url).json()
	poster = User.objects(name=current_user.name).first()
	new_fav = FavoriteBook(author=book_dict["volumeInfo"]["authors"][0], title=book_dict["volumeInfo"]["title"], link=book_url, poster=poster)
	new_fav.save()
	return render_template("confirm.html", api_data=book_dict)


@app.route("/favorites")
@login_required
def favorites():
	current_poster=User.objects(name=current_user.name).first()
	favorites = FavoriteBook.objects(poster=current_poster)
  	return render_template("favorites.html", current_user=current_user, favorites=favorites)


@app.route("/register", methods = ["POST", "GET"])
def register():
	form = UserForm(request.form)
	if request.method == 'POST' and form.validate():
		form.save()
		return redirect("/login")

	return render_template("register.html", form=form)


@app.route("/login", methods = ["GET", "POST"])
def login():
	form = UserForm(request.form)
	if request.method == "POST" and form.validate():
		user = User(name=form.name.data, password = form.password.data)
		login_user(user)
		return redirect('/search')

	return render_template('login.html', form=form)


@app.route("/logout")
def logout():
	logout_user()
	return redirect("/")"""


@app.route("/clusters")
def clusters():
    if request.method == "GET":
        email_id = request.args.get("q")
        if (email_id == ""):
            return redirect("dashgum/login_dashboard?q=clusters")
        client = MongoClient() 
        db = client.bodhi
        cursor = db.user_data.find({"email": email_id})
        if (cursor.count()>0):
            data = {"timestamps":[], "latitudes":[], "longitudes":[], "tz" : []}
            data2 = ""
            data2_for_javascript = ""
            for document in cursor:
                data["timestamps"] = data["timestamps"] + document["timestamp"]                
                data["latitudes"] = data["latitudes"] + document["latitude"]
                data["longitudes"] = data["longitudes"] + document["longitude"]
                data["tz"] = data["tz"] + [str(document["tz"])]
            for i in range(len(data["timestamps"])):
                data["timestamps"][i] = str(data["timestamps"][i])
                data["latitudes"][i] = str(data["latitudes"][i])
                data["longitudes"][i] = str(data["longitudes"][i])
            cursor2 = db.user_processed_data.find({"email": email_id})
            if (cursor2.count()>0):                
                for document in cursor2:
                    data2 = document["clusters"]
                    data2_for_javascript = json.dumps(document["clusters"])
            else:
                data2 = 0
            return render_template("dashgum/cluster_home.html", data = data, data2 = data2, data2_for_javascript=data2_for_javascript)
        else:
            return redirect("/sorry")
        #return render_template("person1.html")

        

        
def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

        
@app.route("/clusters_nearby_places")
def clusters_nearby_places():
    if request.method == "GET":
        latlong = request.args.get("q")
        q = latlong.split(",")
        if (len(q)==2 and isfloat(q[0]) and isfloat(q[1])):
            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+latlong+"&radius=1000&type=restaurant&key=%20AIzaSyAkojborAbwqe8rD9n71cQUnIdRa0VUGU0"            
            restaurants = requests.get(url).json()["results"]
            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+latlong+"&radius=1000&type=grocery_or_supermarket&key=%20AIzaSyAkojborAbwqe8rD9n71cQUnIdRa0VUGU0" 
            stores = requests.get(url).json()["results"]
            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+latlong+"&radius=1000&type=park&key=%20AIzaSyAkojborAbwqe8rD9n71cQUnIdRa0VUGU0" 
            parks = requests.get(url).json()["results"]
            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+latlong+"&radius=1000&type=bank&key=%20AIzaSyAkojborAbwqe8rD9n71cQUnIdRa0VUGU0" 
            banks = requests.get(url).json()["results"]
            data = {"latlong":latlong, "types":[{"type_name":"Restaurants", "type_data":restaurants}, {"type_name":"Parks", "type_data":parks}, {"type_name":"Banks", "type_data":banks}, {"type_name":"Stores", "type_data":stores}]}
            restaurants = json.dumps(restaurants)
            stores = json.dumps(stores)
            parks = json.dumps(parks)
            banks = json.dumps(banks)            
            data_for_javascript = {"latlong":latlong, "types":{"restaurants":restaurants, "stores":stores, "parks":parks, "banks":banks}}            
        else:
            data = 0
        return render_template("dashgum/cluster_nearby_places.html", data = data, data_for_javascript=data_for_javascript)
        #return render_template("person1.html")

@app.route("/sorry")
def not_found():
    return render_template("not_found.html")

@app.route("/")
def home_page():
    return render_template("kasper/index.html")

@app.route("/dashboard")
def dashboard():
    return render_template("dashgum/index.html")

@app.route("/basic_table")
def basic_table():
    return render_template("dashgum/basic_table.html")

@app.route("/blank")
def blank():
    return render_template("dashgum/blank.html")

@app.route("/buttons")
def buttons():
    return render_template("dashgum/buttons.html")

@app.route("/calendar")
def calendar():
    return render_template("dashgum/calendar.html")

@app.route("/chartjs")
def chartjs():
    return render_template("dashgum/chartjs.html")

@app.route("/form_component")
def form_component():
    return render_template("dashgum/form_component.html")

@app.route("/gallery")
def gallery():
    return render_template("dashgum/gallery.html")

@app.route("/general")
def general():
    return render_template("dashgum/general.html")

@app.route("/lock_screen")
def lock_screen():
    return render_template("dashgum/lock_screen.html")

@app.route("/login_dashboard")
def login_dashboard():
    if request.method == "GET":
        page = request.args.get("q")
        return render_template("dashgum/login.html", page = page)

@app.route("/morris")
def morris():
    return render_template("dashgum/morris.html")

@app.route("/panels")
def panels():
    return render_template("dashgum/panels.html")

@app.route("/responsive_table")
def responsive_table():
    return render_template("dashgum/responsive_table.html")

@app.route("/todo_list")
def todo_list():
    return render_template("dashgum/todo_list.html")

@app.route("/neeraj")
def neeraj():
    if request.method == "GET":
        email_id = request.args.get("q")
        limit = request.args.get("limit")        
        if (not(limit)):
            limit = 10  
        limit = int(limit)
        client = MongoClient() 
        db = client.bodhi
        bodies = db.user_data.find({"email": email_id})
        if bodies.count() == 0:
            return redirect("/sorry")
        for body in bodies:
            body["_id"]="xxxxx"
            body = old_db2new_db(body, limit)
            body = json.dumps(body)
            return render_template("neeraj.html", body = body)

@app.route("/neeraj_old")
def neeraj_old():
    if request.method == "GET":
        email_id = request.args.get("q")       
        client = MongoClient() 
        db = client.bodhi
        bodies = db.user_data.find({"email": email_id})
        if bodies.count() == 0:
            return redirect("/sorry")
        for body in bodies:
            body["_id"]="xxxxx"
            body = json.dumps(body)
            return render_template("neeraj.html", body = body)
        
         
"""
template filter to convert timestamp(unix) to date-time in jinja
"""
@app.template_filter('unix_timestamp_to_date_time')
def ts2dt(s):
    return datetime.datetime.fromtimestamp(s)


#function for neeraj's debugging to convert from old style db to new style for better visualization
def old_db2new_db(body, limit):
    new_body = {"locations" : {}}
    latitudes = body["latitude"][-limit:]
    longitudes = body["longitude"][-limit:]
    timestamps = body["timestamp"][-limit:]
    altitudes = body["altitude"][-limit:]
    directions = body["direction"][-limit:]
    speeds = body["speed"][-limit:]
    new_body["email"] = body["email"]
    new_body["deviceId"] = body["deviceId"]
    count = 0
    for timestamp in timestamps:
        new_body["locations"][timestamp] = [latitudes[count], longitudes[count], altitudes[count], speeds[count], directions[count]]
        count = count + 1
    return new_body

if __name__ == "__main__":
	app.run(host="0.0.0.0", port=6006)